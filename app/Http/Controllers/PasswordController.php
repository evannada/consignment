<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PasswordController extends Controller
{

    public function change(Request $request)
    {
        $id = $request->user_id;
        $password = $request->change_password;

        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];
        

        $sendrequest = ['header' => [
                            'id' => 0
                            ],
                        'body' => [
                            'filters' => [
                                'id' => $id
                            ],
                            'attributes' => [
                                'password' => $password
                            ]  
                        ]        
                    ];

        $request = $client->post(config('global.url').'/user/update', [
            'headers' => $headers,
            'json'    => $sendrequest
        ]);

        return response()->json([
            'message' => 'Change password success.'
        ], 200);

    }
}

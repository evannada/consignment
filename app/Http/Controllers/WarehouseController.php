<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class WarehouseController extends Controller
{
    public function getWarehouseGroup()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
         
        $request = $client->post(config('global.url').'/warehouse/group',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [

                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return DataTables::of($data)->make(true);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $request = $client->post(config('global.url').'/user',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [
                    
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return Datatables::of($data)
        ->editColumn('status', function($data){
            if ($data->status == 'ACTIVE') {
                return '<div class="badge badge-success">'. $data->status .'</div>';
            } elseif ($data->status == 'NOT ACTIVE') {
                return '<div class="badge badge-danger">'. $data->status .'</div>';
            }
        })
        ->addColumn('action', function($data){            
            return '<a onclick="editFormUser('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>'.
            '<a onclick="changePassword('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Change Password" data-original-title="Edit"><i class="fas fa-key"></i></a>'.
            '<a onclick="deleteUser('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash"></i></a>';
        })
        ->rawColumns(['status', 'action'])
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fullname' => 'required',
            'username' => 'required',
            'password' => 'required',
            'address'  => 'required',
            'phone'    => 'required',
            'status'   => 'required'
        ]);

        $sendrequest = [ 'body' => [
                            'fullname' => $request->fullname,
                            'username' => $request->username,
                            'password' => $request->password,
                            'address'  => $request->address,
                            'phone'    => $request->phone,
                            'status'   => $request->status
                            ]        
                        ];
        
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
      
        $request = $client->post(config('global.url').'/user/add', [
            'headers' => $headers,
            'json'    => $sendrequest
        ]);

        $data = json_decode($request->getBody());
        
        return response()->json($data->header, 200);
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
        'Authorization' => 'Bearer ' . $token,
        'Content-Type' => 'application/json'
        ];

        $sendrequest = [ 'body' => [
            'id' => $id
            ]        
        ];

        $request = $client->post(config('global.url').'/user', [
            'headers' => $headers,
            'json'    => $sendrequest
        ]);

        $data = json_decode($request->getBody());

        if ($data->header->response_code == 21) {
            return response()->json($data->header, 409);
        }

        return response()->json($data->body[0], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'fullname' => 'required',
            'username' => 'required',
            'address'  => 'required',
            'phone'    => 'required',
            'status'   => 'required'
        ]);
 
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        $sendrequest = [ 'body' => [
                            'filters' => [
                                'id' => $id
                            ],
                            'attributes' => [
                                'fullname' => $request->fullname,
                                'username' => $request->username,
                                'address'  => $request->address,
                                'phone'    => $request->phone,
                                'status'   => $request->status
                            ]  
                        ]        
                    ];

        $request = $client->post(config('global.url').'/user/update', [
                    'headers' => $headers,
                    'json'    => $sendrequest
                ]);

        $data = json_decode($request->getBody());
   
        if ($data->header->response_code == 21) {
            return response()->json($data->header, 409);
        }

        return response()->json($data->body[0], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        $sendrequest = [ 'header' => [
                                'id' => 0
                            ],
                            'body' => [
                                'user_id' => $id
                            ]     
                        ];

        $request = $client->post(config('global.url').'/user/delete', [
                    'headers' => $headers,
                    'json'    => $sendrequest
                ]);

        $data = json_decode($request->getBody());
   
        if ($data->header->response_code == 99) {
            return response()->json($data->header, 200);
        } else {
            return response()->json($data->header, 400);
        }

    }
    
    public function getUser()
    {
        return view('user');
    }
}

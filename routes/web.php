<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
Route::get('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@doLogout')->name('logout');

Route::group(['middleware' => 'auth.apitoken'], function () {

    Route::get('/', 'HomeController@index');

    //admin only
    Route::group(['middleware' => 'admin'], function () {
        Route::get('password', 'PasswordController@getPassword');
        Route::get('user', 'UserController@getUser');
        Route::get('user-role', 'UserRoleController@getUserRole');
        Route::get('user-outlet', 'UserOutletController@getUserOutlet');
        Route::get('article', 'ArticleController@getArticle');
        Route::get('outlet', 'OutletController@getOutlet');
        Route::get('event', 'EventController@getEvent');
        Route::get('edit-sku', 'SalesController@getEditSku');
        Route::get('sales-detail', 'SalesDetailController@getSalesDetail');
        Route::get('item-transfer', 'ItemtransController@getItemtrans');
    });

    Route::get('sales', 'SalesController@getSales');
    Route::get('report/outlet', 'ReportController@getReportSalesOutlet');

});


// API
Route::group(['prefix' => 'api'], function() {
    Route::post('login', 'Auth\LoginController@doLogin');
    Route::put('password', 'PasswordController@change')->name('password.change');
    Route::put('reset/password', 'PasswordController@resetPassword');
    Route::apiResource('user', 'UserController');
    Route::apiResource('user-role', 'UserRoleController');
    Route::apiResource('user-outlet', 'UserOutletController');
    Route::put('user-outlet', 'UserOutletController@update')->name('user-outlet.update');
    Route::apiResource('article', 'ArticleController');
    Route::apiResource('outlet', 'OutletController');
    Route::apiResource('event', 'EventController');
    //Filter
    Route::get('event/brand/{group_ds}', 'EventController@getBrand');
    Route::get('event/brand/{group_ds}/{brand}', 'EventController@getPromo');
    
    Route::get('warehouse/group', 'WarehouseController@getWarehouseGroup');

    Route::apiResource('sales', 'SalesController');
    Route::get('sales-read', 'SalesController@read');
    Route::get('salesdetail/{id}', 'SalesController@detail');
    Route::get('sales-approved', 'SalesController@salesApproved');
    Route::get('report-outlet', 'SalesController@salesOutlet');
    Route::apiResource('sales-detail', 'SalesDetailController');
    Route::apiResource('item-transfer', 'ItemtransController');
    Route::get('sync-outlet', 'SyncController@sync_outlet');
    Route::get('sync-article', 'SyncController@sync_article');
    Route::get('sync-event', 'SyncController@sync_event');
    Route::get('user-outlet/list/user', 'UserOutletController@listUser');
    Route::get('user-role/list/user', 'UserRoleController@listUser');

    // Report
    Route::get('user/report/xls', 'Report\ExcelController@UserReport');
    Route::get('user-outlet/report/xls', 'Report\ExcelController@UserOutletReport');
    Route::get('sales/report/xls', 'Report\ExcelController@SalesReport');
    Route::get('sales/report-outlet/xls', 'Report\ExcelController@SalesReportOutlet');
});

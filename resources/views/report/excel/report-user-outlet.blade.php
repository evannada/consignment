<html>
    <head>
        <style>
            table { border-collapse: collapse; border: 1px solid black; }
        </style>
    </head>
    <body>
        <table width="100%">
            <tr class="header">
                <td>UserID</td>
                <td>Username</td>
                <td>Fullname</td>
                <td>Jenis Outlet</td>
                <td>Kode Outlet</td>
                <td>Nama Outlet</td>
            </tr>
            @foreach ($data as $row)
            <tr class="content">
                <td>{{$row->user_id}}</td>
                <td>{{$row->username}}</td>
                <td>{{$row->fullname}}</td>
                <td>{{$row->jenis_outlet}}</td>
                <td>{{$row->kode_outlet}}</td>
                <td>{{$row->nama_outlet}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>
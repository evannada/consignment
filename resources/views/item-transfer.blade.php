@extends('layouts.master')

@section('content')
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">Consignment</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">Cons</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role') == 'ADMIN')
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> <span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user') }}"></i>User</a></li>
                <li><a class="nav-link" href="{{ url('/user-role') }}"></i>User Rules</a></li>
                <li><a class="nav-link" href="{{ url('/user-outlet') }}"></i>Mapping User - Outlet</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/article') }}" class="nav-link"><i class="fas fa-table"></i><span>Article</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/outlet') }}" class="nav-link"><i class="fas fa-table"></i><span>Outlet</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/event') }}" class="nav-link"><i class="fas fa-table"></i><span>Event</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Sales</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ url('/sales') }}"></i>List Sales</a></li>
            @if(Session::get('role') == 'ADMIN')
            <li><a class="nav-link" href="{{ url('/edit-sku') }}"></i>Edit Sku Event</a></li>
            <li><a class="nav-link" href="{{ url('/sales-detail') }}"></i>Sales Detail</a></li>
            @endif
            </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Report</span></a>
          <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ url('report/outlet') }}"></i>
              Transaction Recap Outlet</a></li>
          </ul>
        </li>
        {{-- <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Stock Moving</span></a>
          <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ url('/item-transfer') }}"></i>Item Transfer</a></li>
          </ul>
        </li> --}}
    </aside>
</div>
      
<!-- Main Content -->
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Item Transfer</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="{{ url('/') }}">Home</a></div>
          <div class="breadcrumb-item">Stock Moving</div>
          <div class="breadcrumb-item">Item Transfer</div>

        </div>
      </div>

      <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <a onclick="refresh()" class="btn btn-outline-warning pull-right">
                      <i class="fas fa-sync-alt"></i>  Refresh</a>
              </div>
              <div class="card-body">
                  <label for="datepicker"><b>Filter :</b></label>
                  <div class="row">
                    <div class="col-md-3 col-sm">
                      <input type="text" class="form-control" name="min" id="min" placeholder="Select Minimum Date">
                    </div>
                    <div class="col-md-3 col-sm">
                        <input type="text" class="form-control" name="max" id="max" placeholder="Select Maximum Date">
                      </div>
                  </div>
                  <br>   
                <div class="table-responsive">
                  <table class="table table-striped" id="item-transfer-datatable">
                    <thead>                                 
                      <tr>
                        <th>Tanggal</th>
                        <th>No. Transaksi</th>
                        <th>No. PO</th>                  
                        <th>No. SJ</th>                  
                        <th>Action</th>                  
                      </tr>
                    </thead>
                    <tbody>                                 
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>


  {{-- Modal --}}
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="tanggal"><b>Tanggal</b></label>
                                <input type="text" class="form-control" id="tanggal" readonly>
                              <label for="PONO"><b>No. PO</b></label>
                                <input type="text" class="form-control" id="PONO" readonly>
                              <label for="gudangin"><b>Gudang In</b></label>
                                <input type="text" class="form-control" id="gudangin" readonly>
                              <label for="keterangan"><b>Keterangan</b></label>
                                <input type="text" class="form-control" id="keterangan" readonly>
                                                         
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="Notrans"><b>No. Transaksi</b></label>
                                <input type="text" class="form-control" id="Notrans" readonly>
                              <label for="SJNO"><b>No. SJ</b></label>
                                <input type="text" class="form-control" id="SJNO" readonly>
                              <label for="gudangout"><b>Gudang Out</b></label>
                                <input type="text" class="form-control" id="gudangout" readonly>
                              <label for="status"><b>Status</b></label>
                                <input type="text" class="form-control" id="status" readonly>                           
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="table-responsive">
                              <table class="table table-striped" id="detail-datatable">
                                <thead>                                 
                                  <tr>
                                    <th>No. Transaksi</th>
                                    <th>Kode Item</th>
                                    <th>Nama Item</th>
                                    <th>Qty</th>
                                  </tr>
                                </thead>
                                <tbody id="records-details">                                 
                                 
                                </tbody>
                              </table>
                            </div>
                      </div>
                    </div>
                </div>
            </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>
{{-- /Modal --}}

@endsection

@section('javascript')

<script type="text/javascript">
var table = $('#item-transfer-datatable').DataTable({
                      processing: true,
                      // serverSide: true,
                      ajax: "{{ url('api/item-transfer') }}",
                      columns: [
                        {data: 'TglTrans', name: 'TglTrans'},
                        {data: 'Notrans', name: 'Notrans'},
                        {data: 'PONO', name: 'PONO'},
                        {data: 'SJNO', name: 'SJNO'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                      ]
                    });

    //Filter Search
    $(function(){
        $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = $('#min').datepicker("getDate");
            var max = $('#max').datepicker("getDate");
            var startDate = new Date(data[0]);
            if (min == null && max == null) { 
              return true; 
              }
            if (min == null && startDate <= max) {
               return true;
               }
            if(max == null && startDate >= min) {
              return true;
              }
            if (startDate <= max && startDate >= min) {
               return true; 
               }
            return false;
          }
        );
            $("#min").datepicker({
              onSelect: function(){
                table.draw();
                }, changeMonth: true, changeYear: true });
            $("#max").datepicker({
              onSelect: function(){
                table.draw(); 
                }, changeMonth: true, changeYear: true });

            // Event listener to the two range filtering inputs to redraw on input
            $('#min, #max').change(function(){
                table.draw();
            });
            
        });

    function refresh() {
        table.ajax.reload();
    }

    function detail(id) {
        $.ajax({
          url: "{{ url('api/item-transfer') }}" + '/' + id,
          type: "GET",
          success: function(response) {
            
            $('#tanggal').val(response.created_date);
            $('#Notrans').val(response.Notrans);

            $('#PONO').val(response.PONO);
            $('#SJNO').val(response.SJNO);

            var gudangin = response.Kdgdgin + ' - ' +  response.NmGdgin;
            var gudangout = response.KdgdgOut + ' - ' +  response.NmGdgOut;

            $('#gudangin').val(gudangin);
            $('#gudangout').val(gudangout);

            $('#keterangan').val(response.Keterangan);
            $('#status').val(response.FlagPosted);

            var data = '';
            $.each(response.details, function(k, v) {
              data += '<tr>' +
                        '<td>' + v.NoTrans + '</td>' +
                        '<td>' + v.KdItem + '</td>' +
                        '<td>' + v.NmItem + '</td>' +
                        '<td>' + v.Qty + '</td>' +
                      '</tr>';
            });
            $('#records-details').html(data);
          },
          error : function() {
              alert("Nothing Data");
          }
        });

        $('#modal-form').modal('show');
        $('.modal-title').text('Detail');
    }
  </script>
    
@endsection
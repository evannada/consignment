@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">Consignment</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">Cons</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown active">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role') == 'ADMIN')
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> <span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user') }}"></i>User</a></li>
                <li><a class="nav-link" href="{{ url('/user-role') }}"></i>User Rules</a></li>
                <li><a class="nav-link" href="{{ url('/user-outlet') }}"></i>Mapping User - Outlet</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/article') }}" class="nav-link"><i class="fas fa-table"></i><span>Article</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/outlet') }}" class="nav-link"><i class="fas fa-table"></i><span>Outlet</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/event') }}" class="nav-link"><i class="fas fa-table"></i><span>Event</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Sales</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ url('/sales') }}"></i>List Sales</a></li>
            @if(Session::get('role') == 'ADMIN')
            <li><a class="nav-link" href="{{ url('/edit-sku') }}"></i>Edit Sku Event</a></li>
            <li><a class="nav-link" href="{{ url('/sales-detail') }}"></i>Sales Detail</a></li>
            @endif
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Report</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ url('report/outlet') }}"></i>
                Transaction Recap Outlet</a></li>
            </ul>
        </li>
        {{-- <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Stock Moving</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ url('/item-transfer') }}"></i>Item Transfer</a></li>
            </ul>
        </li> --}}
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Dashboard</h1>
      </div>

      <div class="section-body">
        <h2 class="section-title">Hi {{ Session::get('username')}}, You are logged in!</h2>
      </div>
    </section>
  </div>
@endsection
    
